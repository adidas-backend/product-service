# product-service

**Pre requisites:-**

- Java Developement IDE
- Maven
- Git

It will call the Live API for products "https://www.adidas.co.uk/api/products/BB5476". and stores the information in cache for faster access.

Product Ids need to be pre added in product table with sample script as :-

_Insert into product values(1,"BB5476")_



**Features :-**

- Caching
- Swagger UI
- Exception Handling
- API Security
- Logging
- Dockerfile
- CI/CD Proposal through Jenkinsfile

**Run Through Docker:-**

1. Building your Docker image

_docker build -t product:0.0.1-SNAPSHOT product/._

2. Running your microservices in Docker containers

_docker run -d --name product -p 8082:8082 product:0.0.1-SNAPSHOT_

**Configure CI/CD Pipeline through Jenkinsfile:-**

Select "Add a Pipeline script from SCM" in Pipeline Section to add the Jenkinsfile that will auto create the stage in the jenkins pipeline.




