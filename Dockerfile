FROM openjdk:8-alpine
WORKDIR /tmp
COPY target/product-0.0.1-SNAPSHOT.jar /tmp/product-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "-Xms128m", "-Xmx256m", "/tmp/product-0.0.1-SNAPSHOT.jar"]
EXPOSE 8082
