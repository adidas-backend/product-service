package com.adidas.product.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.adidas.common.dto.product.Product;
import com.adidas.common.dto.product.ProductResponseDTO;
import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.product.entity.ProductEntity;
import com.adidas.product.repository.ProductRepository;
import com.adidas.product.service.client.ProductApiClient;
import com.adidas.product.service.client.ReviewApiClient;
import com.adidas.product.service.impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
	
	@InjectMocks
	private ProductServiceImpl productService;
	
	@Mock
	private ReviewApiClient reviewApi;
	
	@Mock
	private ProductApiClient productApi;
	
	@Mock
	private ProductRepository productRepo;
	
	@Test
	public void testGetProductDetails() throws Exception {
		
		Product product = mock(Product.class);
		when(productApi.getProductDetails(Mockito.anyString())).thenReturn(product) ;
		ReviewResponseDTO reviewResponseDTO = mock(ReviewResponseDTO.class);
		when(reviewApi.getReviews(Mockito.anyString())).thenReturn(reviewResponseDTO);
		ProductResponseDTO expected = new ProductResponseDTO();
		expected.setProductDetails(product);
		expected.setReviewDetails(reviewResponseDTO);
		ProductResponseDTO res = productService.getProductDetails("ABC123");
		assertEquals(expected, res);
	}
	
	@Test
	public void testCheckIfProductExists() throws Exception {
		ProductEntity product = mock(ProductEntity.class);
		when(productRepo.findByProductId(Mockito.anyString())).thenReturn(product);
		Boolean exists = productService.checkIfProductExists("ABC123");
		assertEquals(Boolean.TRUE, exists);
	}

}
