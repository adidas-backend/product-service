package com.adidas.product.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.adidas.product.service.ProductService;

@RunWith(MockitoJUnitRunner.class)

@WebAppConfiguration
public class ProductControllerTest {
	
	@InjectMocks
	private ProductController productController;

	@Mock
	private ProductService productService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
	}

	@Test
	public void testGetProductDetails() throws Exception {
		
		when(productService.getProductDetails(Mockito.anyString())).thenReturn(Mockito.any());
		mockMvc.perform(MockMvcRequestBuilders.get("/product/AB123")).andExpect(status().isOk());
		
	}
	
	@Test
	public void testCheckIfProductExists() throws Exception {
		when(productService.checkIfProductExists(Mockito.anyString())).thenReturn(Boolean.TRUE);
		mockMvc.perform(MockMvcRequestBuilders.get("/product/AB123/exists")).andExpect(status().isOk());
	}
	
}
