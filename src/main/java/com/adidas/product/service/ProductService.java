package com.adidas.product.service;

import com.adidas.common.dto.product.ProductResponseDTO;

public interface ProductService {
	
	public ProductResponseDTO getProductDetails(String productId);

	Boolean checkIfProductExists(String productId);

}
