package com.adidas.product.service.client;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.adidas.common.dto.error.ErrorCodes;
import com.adidas.common.dto.product.Product;
import com.adidas.common.exception.BaseException;

import reactor.core.publisher.Mono;

@Component
public class ProductApiClient {

	@Cacheable(value = "products", key = "#productId")
	public Product getProductDetails(String productId) {

		WebClient webClient = WebClient.builder().baseUrl("https://www.adidas.co.uk/api/products")
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();

		return webClient.get().uri("/" + productId).retrieve()
				.onStatus(HttpStatus::is4xxClientError,
						clientResponse -> Mono.error(
								new BaseException("Product Not Found", ErrorCodes.NOT_FOUND, HttpStatus.NOT_FOUND)))
				.bodyToMono(Product.class).block();
	}

}
