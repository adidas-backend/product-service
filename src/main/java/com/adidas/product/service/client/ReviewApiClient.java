package com.adidas.product.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.adidas.common.dto.review.ReviewResponseDTO;

@FeignClient(name="reviewApi", url = "${review.api.url}")
public interface ReviewApiClient {
	
	@GetMapping("/review/{productId}")
	public ReviewResponseDTO getReviews(@PathVariable("productId") String productId);

}
