package com.adidas.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adidas.common.dto.product.Product;
import com.adidas.common.dto.product.ProductResponseDTO;
import com.adidas.common.dto.review.ReviewResponseDTO;
import com.adidas.product.repository.ProductRepository;
import com.adidas.product.service.ProductService;
import com.adidas.product.service.client.ProductApiClient;
import com.adidas.product.service.client.ReviewApiClient;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ReviewApiClient reviewApi;
	
	@Autowired
	private ProductApiClient productApi;
	
	@Autowired
	private ProductRepository productRepo;

	@Override
	public ProductResponseDTO getProductDetails(String productId) {
		
		ProductResponseDTO productResp = new ProductResponseDTO();
		Product product = productApi.getProductDetails(productId);
		ReviewResponseDTO reviewRes = reviewApi.getReviews(productId);
		productResp.setProductDetails(product);
		productResp.setReviewDetails(reviewRes);
		return productResp;
		
	}
	
	@Override
	public Boolean checkIfProductExists(String productId) {
		return productRepo.findByProductId(productId)!=null?Boolean.TRUE:Boolean.FALSE;
	}

}
