package com.adidas.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.adidas.common.config.CustomRequestInterceptor;

@SpringBootApplication
@ComponentScan("com")
@EnableFeignClients
@EnableCaching
public class ProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}
	
	@Bean
    public CustomRequestInterceptor basicAuthRequestInterceptor() {
        return new CustomRequestInterceptor();
    }

}
