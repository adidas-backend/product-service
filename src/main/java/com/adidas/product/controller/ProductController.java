package com.adidas.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adidas.common.dto.product.ProductResponseDTO;
import com.adidas.product.service.ProductService;

@RestController
@RequestMapping(path = "/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/{productId}")
	public ResponseEntity<ProductResponseDTO> getProductDetails(@PathVariable("productId") String productId) {
		
		return new ResponseEntity<>(productService.getProductDetails(productId),HttpStatus.OK);
	}
	
	@GetMapping("/{productId}/exists")
	public ResponseEntity<Boolean> checkIfProductExists(@PathVariable("productId") String productId) {
		return new ResponseEntity<>(productService.checkIfProductExists(productId),HttpStatus.OK);
	}

}
